import keyMirror from 'keymirror';

export default keyMirror({
  FETCH_COMMENTS_REQUEST: null,
  FETCH_COMMENTS_SUCCESS: null,
  FETCH_COMMENTS_FAILURE: null,
  MARK_COMMENT_AS_SEEN: null,
});