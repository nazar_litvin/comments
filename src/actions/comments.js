import services from '../services';
import actionTypes from '../constants/actionTypes';

function fetchCommentsRequest() {
  return {
    type: actionTypes.FETCH_COMMENTS_REQUEST,
  }
}

function fetchCommentsSuccess(payload) {
  return {
    type: actionTypes.FETCH_COMMENTS_SUCCESS,
    payload,
  }
}

function fetchCommentsFailure(error) {
  return {
    type: actionTypes.FETCH_COMMENTS_FAILURE,
    error,
  }
}

export function fetchComments() {
  return (dispatch) => {
    dispatch(fetchCommentsRequest());
    
    services
      .fetchComments()
      .then(response => normalizeComments(response.body))
      .then(comments => dispatch(fetchCommentsSuccess(comments)))
      .catch(error => dispatch(fetchCommentsFailure(error)));
  }
}

export function markCommentAsSeen(index) {
  return {
    type: actionTypes.MARK_COMMENT_AS_SEEN,
    payload: {index},
  }
}

/**
 * I like to create some layer between a response from the server
 * and a reducer. Here I can validate and normalize data.
 * And it helps to avoid some problems in the future.
 * For example, if data schema from the BE will change, I can handle it
 * only in one place.
 */
function normalizeComments(comments) {
  return comments.map(comment => ({
    body: comment.body,
    date: comment.dates.created.date_time,
    seen: comment.acknowledged,
    user: {
      fullName: comment.user.full_name,
      avatar: comment.user.image.thumb_url,
    },
  }));
}

