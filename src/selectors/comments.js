import { createSelector } from 'reselect';

export const commentsSelector = state => state.comments;

export const unseenCommentsCountSelector = createSelector(
	commentsSelector,
	comments => comments.data.reduce((mem, comment) => !comment.seen ? mem + 1 : mem, 0)
);