import React from 'react';
import styles from './style.css';
import moment from 'moment';
import PropTypes from 'prop-types';

const Comment = (props) => {
  const {
    markSeen,
    comment: {
      user: { avatar, fullName, },
      body,
      date,
      seen,
    },
  } = props;

  const formattedDate = moment(date, 'DD/MM/YYYY hh:mm')
    .startOf('minute')
    .fromNow();

  const seenBtn = (
    <span className={styles.seenButton} onClick={markSeen}>
      Mark As Seen
    </span>
  );

  return (
    <div className={styles.container}>
      <img src={avatar} className={styles.avatar} alt={fullName}/>
      <div className={styles.message}>
        <p className={styles.userName}>{fullName}</p>
        <p className={styles.body}>{body}</p>
        <p className={styles.date}>{formattedDate} {!seen && seenBtn}</p>
      </div>
    </div>
  );
}

export const commentPropTypes = PropTypes.shape({
  body: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  seen:PropTypes.bool.isRequired,
  user: PropTypes.shape({
    fullName: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
  }),
});

Comment.PropTypes = {
  markSeen: PropTypes.func.isRequired,
  comment: commentPropTypes,
};

export default Comment;
