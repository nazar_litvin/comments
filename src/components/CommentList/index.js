import React from 'react';
import Comment, { commentPropTypes } from '../Comment';
import Button from '../Button';
import styles from './style.css';
import PropTypes from 'prop-types';

const CommentList = (props) => (
  <div className={styles.container}>
    {props.comments.map((comment, index) => (
      <Comment
        comment={comment}
        markSeen={() => props.markSeen(index)}
        key={index}/>
    ))}
    {/*
      I know, I use a div just to wrap a single button,
      it seem's bad for the first look.

      But this way Button component know how doest it should look,
      and know nothing about its position on the page.
      It makes more sense in components based architecture.

      In this case, I want the button to take the whole width of its parent.
     */}
    <div className={styles.footer}>
      <Button
        title='Mark As Approved'
        theme='green'/>
    </div>
  </div>
);



CommentList.propTypes = {
  markSeen: PropTypes.func.isRequired,
  comments: PropTypes.arrayOf(commentPropTypes),
};

export default CommentList;
