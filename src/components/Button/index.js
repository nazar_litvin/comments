import React from 'react';
import styles from './style.css';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const Button = (props) => {
  const {
    title,
    theme,
  } = props;

  const className = classNames(styles.button, {
    [styles.green]: theme === 'green',
  });

  return (
    <a className={className}> 
      {title}
    </a>
  );
}

Button.propTypes = {
  title: PropTypes.string.isRequired,
  theme: PropTypes.string,
};

export default Button;
