import React from 'react';
import styles from './style.css';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const CommentsIndicator = (props) => {
  const { unseenCount, onClick, } = props;
  const hasUnseen = unseenCount > 0;
  /**
   * I know, in this case I can do somethin like this:
   * Boolean(unseenCount), !!unseenCount or just leave unseenCount.
   * 
   * But unseenCount > 0 seems more semantic.
   */

  const buttonClassName = classNames(styles.bellButton, {
    [styles.blue]: hasUnseen,
  });

  return (
    <a className={buttonClassName} onClick={onClick}>
      <i className='fa fa-bell'/>
      { hasUnseen && (
        <span className={styles.unseenCount}>
          {unseenCount}
        </span>
      )}
    </a>
  );
}

CommentsIndicator.propTypes = {
  unseenCount: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default CommentsIndicator;
