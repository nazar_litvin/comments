import actionTypes from '../constants/actionTypes';
import Immutable from 'seamless-immutable';

const INITIAL_STATE = Immutable({
  isFetching: false,
  data: [],
});

const comments = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case actionTypes.FETCH_COMMENTS_REQUEST:
      return state
        .set('isFetching', true);

    case actionTypes.FETCH_COMMENTS_SUCCESS:
      return state
        .set('isFetching', false)
        .set('data', action.payload);

    case actionTypes.FETCH_COMMENTS_FAILURE:
      return state
        .set('isFetching', false);

    case actionTypes.MARK_COMMENT_AS_SEEN:
      return state
        .updateIn(['data', action.payload.index], value =>
          value.set('seen', true));

    default:
      return state;
  }
};

export default comments;