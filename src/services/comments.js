import config from '../lib/config';
import request from 'superagent';

export function fetchComments() {
  return request
    .get(`${config.apiUrl}/167i4f`)
    .set('Accept', 'application/json');
}