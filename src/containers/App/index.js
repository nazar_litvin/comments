import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import actions from '../../actions';
import CommentList from '../../components/CommentList';
import { commentPropTypes } from '../../components/Comment';
import CommentsIndicator from '../../components/CommentsIndicator';
import { commentsSelector, unseenCommentsCountSelector } from '../../selectors/comments';
import styles from './style.css';


class App extends Component {
  static propTypes = {
    fetchComments: PropTypes.func.isRequired,
    markCommentAsSeen: PropTypes.func.isRequired,
    unseenCount: PropTypes.number.isRequired,
    /**
     * Reusing property types like commentPropTypes help to avoid
     * code duplication.
     * 
     * We can use Flow for type checking as well.
     * It helps catch errors in a team environment,
     * like "expected string you're using number",
     * Also, Flow helps to make good self-documenting code.
     */
    comments: PropTypes.shape({
      isFetching: PropTypes.bool.isRequired,
      data: PropTypes.arrayOf(commentPropTypes),
    }),
  };

  state = {
    /**
     * This can be done through the redux as well.
     * Some kind information we can put to the component's state.
     * This usually depends on the project and use cases.
     */
    showComments: false,
  };

  componentDidMount() {
    this.props.fetchComments();
  }

  render() {
    /**
     * A comma after the last item in an object 
     * helps to avoid some git merge conflicts
     */
    const { unseenCount } = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.header}> 
          <CommentsIndicator
            onClick={this.handleCommentsClick.bind(this)}
            unseenCount={unseenCount}/>
        </div>
        <div className={styles.content}>
          {this.renderCommentList()}
        </div>
      </div>
    );
  }

  handleCommentsClick() {
    this.setState({
      showComments: !this.state.showComments,
    });
  }

  renderCommentList() {
    const { comments, markCommentAsSeen } = this.props;

    if (!this.state.showComments) {
      return null; 
    }

    return (
      <CommentList
        markSeen={markCommentAsSeen}
        comments={comments.data}/>
    );
  }
}

const select = createSelector(
  commentsSelector,
  unseenCommentsCountSelector,
  (comments, unseenCount) => ({comments, unseenCount}),
)

const bindActions = {
  fetchComments: actions.fetchComments,
  markCommentAsSeen: actions.markCommentAsSeen,
};

export default connect(select, bindActions)(App);
