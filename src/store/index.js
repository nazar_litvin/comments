import { createStore, applyMiddleware } from 'redux';
import reduxThunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';

const store = createStore(
  rootReducer,
  applyMiddleware(
    reduxThunkMiddleware,
    createLogger(),
  ),
);

export default store;