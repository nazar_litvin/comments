import React from 'react';
import ReactDOM from 'react-dom';
import Router from './router';
import { Provider as ReduxProvider} from 'react-redux';
import store from './store';

ReactDOM.render(
  <ReduxProvider store={store}>
    <Router/>
  </ReduxProvider>,
  document.getElementById('root')
);
